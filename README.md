# What the hack is phanatic?

Phanatic is a programming language, that transpiles to PHP. It's like what Typescript is for Javascript, just for PHP.

# Features

| Feature           |Implemented|
|-------------------|---|
| Generic functions |No|
| Generic classes | No|
| Typed arrays      |No|
| Async support     |No|
| Class extensions  | No|

# Installation

```
composer require --dev phanatic/phanatic
```

# Usage

The fastest way to start developing is to run the watcher:

```
vendor/bin/phanatic watch
```

# Performance

## Compile time performance

TODO

## Runtime performance

TODO

